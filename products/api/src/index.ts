import { Queue, QueueEvents, Job, JobsOptions} from 'bullmq';

import * as T from './types';

export const name = "users";
export const version: number = 2;

export const Create = async (props: T.Create.Request, redis: T.REDIS, opts: JobsOptions): Promise<T.Create.Response> => {

    try {

        const queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvent: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

        const endPoint: T.Endpoint =  'create';


        const job = await queue.add(endPoint, props, opts);


        await job.waitUntilFinished(queueEvent);

        const result = await Job.fromId(queue, job.id);


        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message };
        
    } catch (error) {

        throw { statusCode: 'error', message: error.toString() };

    }
    
};

export const Delete = async (props: T.Delete.Request, redis: T.REDIS, opts: JobsOptions): Promise<T.Delete.Response> => {

    try {

        const queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvent: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

        const endPoint: T.Endpoint =  'delete';


        const job = await queue.add(endPoint, props, opts);


        await job.waitUntilFinished(queueEvent);

        const result = await Job.fromId(queue, job.id);


        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message };
        
    } catch (error) {

        throw { statusCode: 'error', message: error.toString() };

    }
    
};

export const Update = async (props: T.Update.Request, redis: T.REDIS, opts: JobsOptions): Promise<T.Update.Response> => {

    try {

        const queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvent: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

        const endPoint: T.Endpoint =  'update';


        const job = await queue.add(endPoint, props, opts);


        await job.waitUntilFinished(queueEvent);

        const result = await Job.fromId(queue, job.id);


        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message };
        
    } catch (error) {

        throw { statusCode: 'error', message: error.toString() };

    }
    
};

export const FindOne = async (props: T.findOne.Request, redis: T.REDIS, opts: JobsOptions): Promise<T.findOne.Response> => {

    try {

        const queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvent: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

        const endPoint: T.Endpoint =  'findOne';


        const job = await queue.add(endPoint, props, opts);


        await job.waitUntilFinished(queueEvent);

        const result = await Job.fromId(queue, job.id);


        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message };
        
    } catch (error) {

        throw { statusCode: 'error', message: error.toString() };

    }
    
};

export const View = async (props: T.View.Request, redis: T.REDIS, opts: JobsOptions): Promise<T.View.Response> => {

    try {

        const queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvent: QueueEvents = new QueueEvents(`${name}:${version}`, { connection: redis });

        const endPoint: T.Endpoint =  'view';


        const job = await queue.add(endPoint, props, opts);


        await job.waitUntilFinished(queueEvent);

        const result = await Job.fromId(queue, job.id);


        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message };
        
    } catch (error) {

        throw { statusCode: 'error', message: error.toString() };

    }
    
};
