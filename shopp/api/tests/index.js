const api = require('../dist');

const redis = {

    local: {
        host: "192.168.0.106",
        port: 6379,
        //password: undefined
    }
};

const enviroment = redis.local;

const users = [
    {
        fullName: 'Ricardo Jose',
        image: 'https://cdn.pixabay.com/photo/2018/03/07/17/15/balloon-3206530_960_720.jpg',
        phone: '555555',
        username: 'ricardotg'
    },

    {
        fullName: 'laura',
        image: 'https://cdn.pixabay.com/photo/2018/03/07/17/15/balloon-3206530_960_720.jpg',
        phone: '555557',
        username: 'laurap'
    }
];

async function create(user) {

    try {

        const result = await api.Create(user, enviroment);

        console.log(result);
        
        
    } catch (error) { console.error(error); }
    
};

async function del(username) {
    
    try {

        const result = await api.Delete({username}, enviroment);

        console.log(result);
        
        
    } catch (error) { console.error(error); }
};

async function update(params) {

    try {

        const result = await api.Update(params, enviroment);

        console.log(result);
        
        
    } catch (error) { console.error(error); }    
    
};

async function findOne(params) {

    try {

        const result = await api.FindOne(params, enviroment);

        console.log(result);
        
        
    } catch (error) { console.error(error); }
    
};

async function view(params) {

    try {

        const result = await api.View(params, enviroment);

        console.log(result);
        
        
    } catch (error) { console.error(error); }
    
};

const main = async () => {

    try {

        await view();

        //await create(users[0]);

        //await view();

        //await del(users[0].username);

        await view();

        
    } catch (error) {
        console.error(error);
    }
}

main()