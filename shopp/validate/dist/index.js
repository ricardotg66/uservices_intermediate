"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOne = exports.view = exports.update = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
async function create(params) {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
            fullName: joi_1.default.string().required(),
            phone: joi_1.default.number().required(),
            image: joi_1.default.string().uri().required()
        });
        const result = await schema.validateAsync(params);
        if (params.username === 'admin' || 'root' || 'su') {
            throw { statusCode: 'error', message: "Nombre reservador para uso interno" };
        }
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
exports.create = create;
;
async function del(params) {
    try {
        const schema = joi_1.default.object({ username: joi_1.default.string().required() });
        const result = await schema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
exports.del = del;
;
async function update(params) {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
            fullName: joi_1.default.string(),
            phone: joi_1.default.number(),
            image: joi_1.default.string()
        });
        const result = await schema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
exports.update = update;
;
async function view(params) {
    try {
        const schema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            state: joi_1.default.boolean()
        });
        const result = await schema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
exports.view = view;
;
async function findOne(params) {
    try {
        const schema = joi_1.default.object({ username: joi_1.default.string().required() });
        const result = await schema.validateAsync(params);
        return { statusCode: 'success', data: params };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
}
exports.findOne = findOne;
;
//# sourceMappingURL=index.js.map