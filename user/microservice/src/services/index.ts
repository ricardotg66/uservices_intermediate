import * as Models from "../models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from '../../../validate/dist';

export async function create(params: T.Services.Create.Request): Promise < T.Services.Create.Response > {

    try {

        await Validation.create(params);

        const findOne = await Models.findOne({ where: { username: params.username } });

        if (findOne.statusCode !== 'notFound') {

            switch (findOne.statusCode) {

                case 'success': return { statusCode: 'ValidationError', message: "Usuario ya resgistrado" };
            
                default: return { statusCode: 'error', message: InternalError }
                    
            };

        };

        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service create", error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
    
    
};

export async function del(params: T.Services.Delete.Request): Promise < T.Services.Delete.Response > {

    try {

        await Validation.del(params);

        var where: T.Models.where = { username: params.username };

        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== 'success') {

            switch (findOne.statusCode) {

                case 'notFound': return { statusCode: 'ValidationError', message: "Usuario no encontrado" };
            
                default: return { statusCode: 'error', message: InternalError };
                    
            };

        };

        const { statusCode, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: findOne.data };

    } catch (error) {

        console.error({ step: "Service delete", error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
    
};

export async function update(params: T.Services.Update.Request): Promise < T.Services.Update.Response > {

    try {

        await Validation.update(params);

        var where: T.Models.where = { username: params.username };

        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== 'success') {

            switch (findOne.statusCode) {

                case 'notFound': return { statusCode: 'ValidationError', message: "Usuario no esta resgistrado" };
            
                default: return { statusCode: 'error', message: InternalError };
                    
            };

        };

        if (!findOne.data.state) {

            return { statusCode: 'notPermitted', message: "Tu usuario no esta habilitado" };
        }

        const { statusCode, data, message } = await Models.update(params, { where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data[1][0] };

    } catch (error) {

        console.error({ step: "Service delete", error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }
    
};

export async function view(params: T.Services.View.Request): Promise < T.Services.View.Response > {

    try {

        await Validation.view(params);

        var where: T.Models.where = { };

        var optionals: T.Models.Attributes[] = ['state'];

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service view", error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }  
};

export async function findOne(params: T.Services.findOne.Request): Promise < T.Services.findOne.Response > {

    try {

        await Validation.findOne(params);

        var where: T.Models.where = { username: params.username };

        const { statusCode, data, message } = await Models.findOne({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: "Service findOne", error: error.toString() });

        return { statusCode: 'error', message: InternalError };
    }  
};

