import * as S from 'sequelize';

export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export namespace Models {

    export interface ModelAttributes {

        id?: number;

        username?: string;

        password?: string;

        fullName?: string;

        image?: string;

        state?: boolean;

        phone?: number;

        createAt?: string;

        updateAt?: string;
    };

    export const attributes = ['id', 'username', 'password', 'fullname', 'image', 'state', 'phone', 'createAt', 'updateAt'] as const;

    export type Attributes = typeof attributes[number];

    export type where = S.WhereOptions<ModelAttributes>;

    export interface Model extends S.Model<ModelAttributes> { };

    export interface Paginate {
        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    }

    export namespace SyncDB {

        export interface Request extends S.SyncOptions {

        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        } 
    };

    export namespace Count {

        export interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    }

    export namespace Create {

        export interface Request extends ModelAttributes {

        };

        export interface Opts extends S.CreateOptions<ModelAttributes> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        };
    }

    export namespace Delete {

        export interface Opts extends S.DestroyOptions<ModelAttributes> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    }

    export namespace FindAndCountAll {

        export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, 'group'> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        };
    }

    export namespace FindOne {

        export interface Opts extends S.FindOptions<ModelAttributes> { }
        export interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        };
    }
    
    export namespace Update {

        export interface Request extends ModelAttributes {

        };

        export interface Opts extends S.UpdateOptions<ModelAttributes> { }
        export interface Response {
            statusCode: statusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        };
    }   


};
/*interface Servicio {
    fullName: string,
    phone: number,
    image: string
};

interface Adaptador {
    fullName: string,
    phone: string,
    image: string
}*/

export namespace Services {

    export namespace findOne {

        export interface Request {
            username: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }

    }

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;

            state?: boolean;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        }

    }

    export namespace Update {

        export interface Request {
            username: string;
            fullName?: string;
            phone?: number;
            image?: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }

    }
    export namespace Create {
        export interface Request {
            username: string;
            fullName: string;
            phone: number;
            image: string;
        }
    
        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }

    export namespace Delete {
        export interface Request {

            username: string;
        }
    
        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
};

export namespace Controller {

    export namespace Publish {
        
        export interface Request {
            channel: string;
            instance: string;
        };

        export interface Response {
            statusCode: statusCode;
            data?: Request;
            message?: string;
        };
    }
   
};

export namespace Adapters {

    export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {

        export interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}

export namespace Settings {

    /*export interface DATABASE {
        name: string;
        user: string;
        password: string;
        host: string;
        port: number;
    }*/

    export const attributes = ['create', 'update', 'delete', 'findOne', 'view'] as const;

    export type Attributes = typeof attributes[number];

    export interface REDIS {
        host: string;
        port: number;
        password: string;
    }
    
};